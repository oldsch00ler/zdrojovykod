﻿############################
# GENERATOR HESEL          #
# Autor: Lukáš Zuzaňák     #
############################
 
#Nastaveni domovskeho adresare pro POWERSHELL aby se mohl zkompilovat do EXE
  $scriptRoot = [System.AppDomain]::CurrentDomain.BaseDirectory.TrimEnd('\')
  if ($scriptRoot -eq $PSHOME.TrimEnd('\'))
  {
    $scriptRoot = $PSScriptRoot
  }
 
#Vstupni hodnoty
write-host "GENERATOR HESLA by Lukas Zuzanak" -ForegroundColor Yellow
write-host "--------------------------------" -ForegroundColor Yellow
write-host ""
$pwdleng = read-host "Delka hesla"
 
#ZACATEK - generovani hesla
  $Private:OFS = "" #nastaveni separatoru
  $PasswordLength = $pwdleng #delka hesla
  $InclChars = ‘abcdefghkmnprstuvwxyzABCDEFGHKLMNPRSTUVWXYZ123456789#&@$€-_?!’ #pouzite znaky pro heslo
  $RandomNums = 1..$PasswordLength | ForEach-Object { Get-Random -Maximum $InclChars.length } #pouziti nahodnych znaku
  $RandomPassword = [String]$InclChars[$RandomNums] #nacteni hesla do promenne
#KONEC - generovani hesla
 
#Vystup
write-host ""
write-host "--------------------------------" -ForegroundColor Yellow
write-host "Heslo:" $RandomPassword -ForegroundColor Cyan