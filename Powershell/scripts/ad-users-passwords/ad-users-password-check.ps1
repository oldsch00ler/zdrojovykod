<#
    AUTOR: Lukáš Zuzaňák
    VERZE: v1r1
#>
 
# Zjištění kdo si nikdy nezměnil heslo
Get-ADUser -Filter * -Properties CannotChangePassword | 
where {$_.CannotChangePassword} | 
sort-object {$_.samAccountName} | 
Select samAccountName | 
Export-csv -path c:\scripts\CHU_users-password-never-changed.csv
 
# Zjištění stáří hesla
Get-ADUser -filter * -properties passwordlastset, passwordneverexpires | 
sort-object Surname | 
select-object SamAccountName, Enabled, PasswordLastSet, PasswordNeverExpires | 
Export-csv -path c:\scripts\CHU_all-users-password-info.csv
 
# Zjištění stáří hesla jen u povolených účtů
Get-ADUser -filter * -properties passwordlastset, passwordneverexpires | 
sort-object Surname | 
select-object SamAccountName, Enabled, PasswordLastSet, PasswordNeverExpires | 
where enabled -like "true" | 
Export-csv -path c:\scripts\CHU_enabled-users-password-info.csv
 
# === Zjištění kdo má heslo starší než 1 rok ===
# Porovnávací datum (- 1 rok)
$datumP = (Get-Date).AddYears(-1)#.ToString("dd/MM/yyyy")
 
# Nacteni povolenych uzivatelu
$users = Get-ADUser -filter * -properties passwordlastset, passwordneverexpires | 
sort-object Surname | 
select-object SamAccountName, Enabled, PasswordLastSet, PasswordNeverExpires | 
where {$_.enabled -like "true" -And $_.PasswordLastSet -ne $Null -And $_.PasswordLastSet -lt $datumP} |
Export-csv -path c:\scripts\CHU_users-old-password-info.csv
 
# === Poslani varovneho emailu ===
# Konfigurace emailu
$location = "c:\scripts\"
$From = "powershell@email.cz"
$To = "admin@email.cz"
$Subject = "HESLA AD UZIVATELU"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "587"
$Body = "POZOR! Probehla diagnostika uzivatelu domeny a je mozne, ze nekteri maji stare hesla! Bez a zkontroluj to... --> Logy zde: $location"
 
$User = "powershell@email.cz"
$Pass = ConvertTo-SecureString ‘<super_tajne_heslo>’ -AsPlainText -Force
$Cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User,$Pass
 
# Poslání emailu (je třeba ověření přes google účet!)
Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -UseSsl -Credential $Cred